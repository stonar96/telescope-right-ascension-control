# telescope-right-ascension-control

Simple control of the right ascension of an telescope with an Atmel AVR microcontroller and a stepper motor.

## Usage
- _system_ on / off switch: power the system (no support in the microcontroller needed)
- _follow_ on / off switch: turn right ascension with the programmed speed
- _speed_ moon / stars switch: use the angular speed of the moon or of the stars
- _override enable_ on / off switch: enable override control via potentiometer.
- _override_ potentiometer: control the speed of the stepper manually, the control value is added to the follow speed.
    The potentiometer is calibrated each time _override enable_ is turned on.

## Future features

### Better stepper motor control
- half steps
- automatically turn off stepper if it is standing still

### Direction switch
- switch to change running direction at runtime

### Better manual control
- _dead zone_: minimal input does nothing to work around change of potentiometer rest position
- nonlinear override: fine control at slow speeds and fast maximum speed

### Out of source build
- seperate build and source files

## Possible extended features

### Program mode
- either as compile time option or as switch
- used to "train" the system to the desired speed
- saves a correction factor in the EEPROM

### Test photo mode
- rotate with maximum speed for some time
- if a picture is shot during that time and the right ascension axis is correctly set up, every star except Polaris should show trails.
