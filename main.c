/* Simple control of the right ascension of an telescope with an Atmel AVR 
 * microcontroller and a stepper motor. See README.md for more information.
 * Author: lukjp
 */
#include <stdint.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "SSSC.h"
#include "config.h"
#include "ADC.h"

// add and set overflows to max value
int32_t safeAdd (int32_t a, int32_t b);

int main(void) {
    // change clock prescaler to 1, to allow usage with default fuses
    CLKPR = (1 << 7);
    CLKPR = 0;
    
    // configure in- and outputs
    FOLLOW_SWITCH_PORT |= (1 << FOLLOW_SWITCH_PIN_NR);
    MOON_SWITCH_PORT |= (1 << MOON_SWITCH_PIN_NR);
    OVERRIDE_ENABLE_PORT |= (1 << OVERRIDE_ENABLE_PIN_NR);
    
    SSSC_setup();
    
    ADC_Init(ADC_PRESCALER_64, ADC_NOT_LEFT_ADJUSTED, ADC_REF_VOLTAGE_AVCC, ADC_AUTOTRIGGER_DISABLED, ADC_INTERRUPT_DISABLED);
    ADC_Enable();
    
    sei();
    
    uint16_t speedOverrideZeroPoint;
    uint8_t speedOverrideWasEnabled = 0;
    
    while (1) {
        int32_t speed = 0;
        if (!(FOLLOW_SWITCH_PIN & (1 << FOLLOW_SWITCH_PIN_NR))) {
            if (!(MOON_SWITCH_PIN & (1 << MOON_SWITCH_PIN_NR))) {
                speed = MOON_STEPPER_SPEED;
            } else {
                speed = STARS_STEPPER_SPEED;
            }
        }
        
        if (!(OVERRIDE_ENABLE_PIN & (1 << OVERRIDE_ENABLE_PIN_NR))) {
            if (speedOverrideWasEnabled) {
                uint16_t ADCResult;
                ADC_Measure_10bit_nTimesSum(&ADCResult, 4);
                int16_t controldifference = ((int16_t) ADCResult) - speedOverrideZeroPoint;
                int32_t speeddifference = (((int32_t) controldifference ) * OVERRIDE_MAX_SPEED ) / (1024 * 4);
                speed = safeAdd(speed, speeddifference);
            } else {
                speedOverrideWasEnabled = 1;
                ADC_Measure_10bit_nTimesSum(&speedOverrideZeroPoint, 16);
                speedOverrideZeroPoint /= 4;
            }
        } else {
            speedOverrideWasEnabled = 0;
        }
        SSSC_setSpeed(-speed);
    }
}

int32_t safeAdd (int32_t a, int32_t b) {
    int64_t sum = (((int64_t) a) + b);
    if (sum > INT32_MAX) {
        sum = INT32_MAX;
    }
    if (sum < INT32_MIN) {
        sum = INT32_MIN;
    }
    return (int32_t) sum;
}
