/*
 * Small and simple library for handling an unipolar stepper motor.
 * Author: lukjp
 */

#include "stepper.h"

uint8_t StepperCreateConfig(struct StepperConfig *config, uint32_t maxSpeed, uint32_t maxAccel, uint32_t recalcTime) {
    uint8_t returnValue = 0;
    // *1000 because maxSpeedDifference is in steps per 1000 seconds
    // /F_CPU because recalcTime is in ticks
    uint64_t maxSpeedDifference = (((uint64_t) maxAccel) * recalcTime * 1000) / F_CPU;
    if (maxSpeedDifference > INT32_MAX) {
        returnValue = 1;
        maxSpeedDifference = INT32_MAX;
    }
    config->maxSpeedDifference = maxSpeedDifference;

    if (maxSpeed > INT32_MAX) {
        returnValue = 1;
        maxSpeed = INT32_MAX;
    }
    config->maxSpeed = maxSpeed;
    return returnValue;
}


uint8_t StepperInit(struct StepperConfig *config, struct Stepper *stepper, volatile uint8_t * port, uint8_t coilPinBits[4]) {
    stepper->config = config;
    stepper->port = port;
    stepper->currSpeed = 0;
    stepper->setSpeed = 0;
    stepper->state =0;
    uint8_t i;
    for (i = 0; (i < 4) && (coilPinBits[i] < 8); i++) {
        stepper->coilPinBits[i] = coilPinBits[i];
    }
    if (i != 4) {
        return 1;
    }
    else {
        return 0;
    }
}


uint8_t StepperSetSpeed(struct Stepper *stepper, int32_t speed) {
    uint8_t returnValue = 0;
    uint32_t setSpeed;
    if (speed > ((int32_t) stepper->config->maxSpeed)) {
        setSpeed = stepper->config->maxSpeed;
        returnValue = 1;
    }
    else if (speed < (-((int32_t) stepper->config->maxSpeed ))) {
        setSpeed = -stepper->config->maxSpeed;
        returnValue = 1;
    }
    else {
        setSpeed = speed;
        returnValue = 0;
    }
    uint8_t tmpSREG = SREG;
    cli();
    stepper->setSpeed = setSpeed;
    SREG = tmpSREG;
    return returnValue;
}


struct StepperStepTime StepperRecalc(struct Stepper *stepper) {
    struct StepperStepTime stepperStepTime;
    //create local copies because of volatile
    int32_t setSpeed = stepper->setSpeed;
    int32_t currSpeed = stepper->currSpeed;
    if (setSpeed == currSpeed) {
        // We do not need to to anything
        stepperStepTime.stepTimeMode = keepStepTime;
    }
    else {
        // calculate new speed
        int32_t newSpeed;
        if (setSpeed > currSpeed) {
            newSpeed = currSpeed + stepper->config->maxSpeedDifference;
            if (newSpeed < currSpeed) {
                // integer overflow, set new Speed to integer maximum
                newSpeed = INT32_MAX;
            }
            if (newSpeed > setSpeed) {
                // we are at the end of acceleration
                newSpeed = setSpeed;
            }
        }
        else {
            // setSpeed < currSpeed
            newSpeed = currSpeed - stepper->config->maxSpeedDifference;
            if (newSpeed > currSpeed) {
                // integer overflow, set new Speed to integer minimum
                newSpeed = INT32_MIN;
            }
            if (newSpeed < setSpeed) {
                // we are at the end of acceleration
                newSpeed = setSpeed;
            }
        }
        
        // write new speed
        uint8_t tmpSREG = SREG;
        cli();
        stepper->currSpeed = newSpeed;
        SREG = tmpSREG;
        
        // calculate step time
        // from now on, we are only interrested in the absolute vale for calculating the step time
        uint32_t absSpeed = (newSpeed < 0 ? -newSpeed : newSpeed);
        if (absSpeed == 0) {
            stepperStepTime.stepTimeMode = noStep;
        }
        else {
            // if F_CPU is to large, we split the multiplication to avoid a 64 bit division
#if (F_CPU * 1000) > INT32_MAX
            stepperStepTime.stepTime = ((F_CPU * 125) / absSpeed) * 8;
#else
            stepperStepTime.stepTime = (F_CPU * 1000) / absSpeed;
#endif
            stepperStepTime.stepTimeMode = updateStepTime;
        }
    }
    return stepperStepTime;
}


void StepperStep(struct Stepper *stepper) {
    int32_t currSpeed = stepper->currSpeed;
    if (currSpeed != 0) {
        uint8_t state = stepper->state;
        // switch off old coil
        * (stepper->port) &= ~(1 << stepper->coilPinBits[state]);
        if (currSpeed >= 0) {
            state = (state + 1) % 4;
        }
        else {
            // we need this cast, else we would get from 0 to 255
            state = ((uint8_t) (state - 1)) % 4;
        }
        // switch on new coil
        * (stepper->port) |= (1 << stepper->coilPinBits[state]);
        stepper->state = state;
    }
    return;
}
