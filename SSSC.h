/* SSSC: Simple Single Stepper Controller
 * Very simple to use controller for a stepper, works on ATMega8, ATMega328 and
 * ATMega328p.
 * It uses Timer0 for acceleration control and Timer1 as step timer.
 * Usage:
 *  - call SSSC_setup() to set up stepper
 *  - call SSSC_setSpeed(int32_t speed) to set the speed of the motor
 * 
 * TODO:
 *  - implement switching of step timer prescaler for greater resolution and
 *      range of the step time
 *  - enable stepping interrupt during step time recalculation to reduce jitter
 *  - add support for turning the stepper on or off
 * 
 * Author: lukjp
 */


#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "stepper.h"

#ifndef SSSC_H_
#define SSSC_H_

/* NOTE: The following constants can be changed */

/* port and pins where the stepper is connected */
#define STEPPER_PORT PORTD
#define STEPPER_DDR DDRD
#define STEPPER_PIN_0 0
#define STEPPER_PIN_1 1
#define STEPPER_PIN_2 2
#define STEPPER_PIN_3 3

/* maximum speed of the stepper in steps per 1000 seconds */
#define STEPPER_MAX_SPEED 1000000UL
/* maximum acceleration of the stepper in (steps per second) per second */
#define STEPPER_MAX_ACCEL 1000UL


/* NOTE: The following constants should not be changed*/

#define RECALC_TIMER_PRESCALER 256UL
#define RECALC_TIMER_TOP 255UL

#define RECALC_TIME (RECALC_TIMER_PRESCALER * (RECALC_TIMER_TOP + 1))

/* setup stepper with timing */
void SSSC_setup(void);

/* Set  speed of the right ascension motor.
 * Parameters:
 *  - int32_t speed : new speed for the motor in steps per 1000 seconds */
void SSSC_setSpeed(int32_t speed);


#endif // SSSC_H_
