/*
 * Small and simple library for handling an unipolar stepper motor.
 * Currently only jog mode is supported, which means constant velocity
 * Terminology:
 *  - ticks: CPU-Ticks
 *  - state: which coil is currently active
 *  - step: one coil forward (ex.: from A1 to B1)
 * Architecture:
 *  At start a configuration has to be created with StepperCreateConfig, then a
 *  stepper must be initialized with an config. Now the target speed can be
 *  controlled by StepperSetSpeed.
 *  For speed control StepperRecalc must be called regularly to accelerate.
 *  It returns what should be done and the time between the steps. If the
 *  stepper is running, StepperStep need then be called with this interval.
 * 
 * TODO:
 *  - add support for disabling the stepper
 * Author: lukjp
 */


#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#ifndef STEPPER_H_
#define STEPPER_H_

struct StepperConfig { // configuration for stepper motor, can be shared between more
    uint32_t maxSpeed; // maximum speed in steps per 1000 seconds
    uint32_t maxSpeedDifference; // maximum by which the speed can increase or decrease per recalc
};

struct Stepper { // data for each stepper, unique to each stepper
    volatile int32_t setSpeed; // target speed for the motor in steps per 1000 seconds
    volatile int32_t currSpeed; // current speed in in steps per 1000 seconds
    volatile uint8_t state; // currently active coil
    volatile uint8_t * port; // address of the port where the stepper is connected
    uint8_t coilPinBits[4]; // bit numbers of the pins where the coils are connected
    struct StepperConfig * config; // configuration to use
};

enum StepTimeMode { // what should be done to the step timer
    noStep, // stepper is stopped, StepperStep need not be called
    keepStepTime, // keep step time, nothing need to be done
    updateStepTime // update the step time
};

struct StepperStepTime {
    enum StepTimeMode stepTimeMode; // what should be done
    uint32_t stepTime; // time between Steps in ticks if changed
};


/* Create a new config for a stepper motor
 * Parameters:
 *  - struct StepperConfig *config  : Config to create
 *  - uint32_t maxSpeed             : maximum speed in steps per 1000 seconds
 *  - uint32_t max_Accel            : maximum acceleration in (steps per second) per second
 *  - uint32_t recalcTime           : time between calls of the recalc Function in ticks
 * return 0: stepper config could be created
 * return 1: one or more parameters are not possible
 */
uint8_t StepperCreateConfig(struct StepperConfig *config, uint32_t maxSpeed, uint32_t maxAccel, uint32_t recalcTime);


/* initialize a new stepper
 * Parameters:
 *  - struct StepperConfig *config  : address of the config to use by the new stepper
 *  - struct Stepper *stepper       : address of the data of the new stepper
 *  - uint8_t * Port                : address of the PORT to use
 *  - uint8_t coilPinBits[4]        : Pin bit numbers of the Pins which are connected to the coils
 *                                      (ex.: {0, 1, 2, 3} if PORTX0, PORTX1, PORTX2, PORTX3, are used)
 * return 0: stepper initialized properly
 * return 1: one or more parameters are not possible
 */
uint8_t StepperInit(struct StepperConfig *config, struct Stepper *stepper, volatile uint8_t * port, uint8_t coilPinBits[4]);


/* Set  target speed of the motor.
 * Parameters:
 *  - struct Stepper *stepper       : address of the data of the stepper
 *  - int32_t speed                 : new speed for the motor in steps per 1000 seconds
 * return 0: speed set
 * return 1: absolute speed above maximum speed, speed set to maximum
 */
uint8_t StepperSetSpeed(struct Stepper *stepper, int32_t speed);


/* Recalculate time between Steps
 * Parameters:
 *  - struct Stepper *stepper       : address of the data of the stepper
 * return value: stepperStepTime, see above
 */
struct StepperStepTime StepperRecalc(struct Stepper *stepper);


/* Step one step forward or backward if speed is not zero
 *  - struct Stepper *stepper       : address of the data of the stepper
 */
void StepperStep(struct Stepper *stepper);


#endif // STEPPER_H_
