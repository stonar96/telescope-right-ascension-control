/* SSSC: Simple Single Stepper Controller
 * Author: lukjp
 */

#include "SSSC.h"

static struct StepperConfig stepperConfig;
static struct Stepper stepper;

void SSSC_setup(void) {
    STEPPER_DDR |= ( 1 << STEPPER_PIN_0 ) | ( 1 << STEPPER_PIN_1 ) | ( 1 << STEPPER_PIN_2 ) | ( 1 << STEPPER_PIN_3 );
    StepperCreateConfig(&stepperConfig, STEPPER_MAX_SPEED, STEPPER_MAX_ACCEL, RECALC_TIME);
    uint8_t stepperPins[4] = {STEPPER_PIN_0, STEPPER_PIN_1, STEPPER_PIN_2, STEPPER_PIN_3};
    StepperInit(&stepperConfig, &stepper, &STEPPER_PORT, stepperPins);
    
    // Setup recalc timer: clk/256, output compare disconnected and normal mode
    // Enable recalc timer overflow interrupt
#if defined ( __AVR_ATmega328P__ ) || defined ( __AVR_ATmega328__ )
    TCCR0A = 0;
    TCCR0B = ( 1 << CS02 );
    TIMSK0 = ( 1 << TOIE0 );
#elif defined  (__AVR_ATmega8__ )
    TCCR0 = ( 1 << CS02 );
    TIMSK |= ( 1 << TOIE0 );
#endif
    
    // Setup step Timer: fast pwm mode with OCR1A as top, output compare disconnected and clk/64
    // In this mode we can change the frequency at any time, without danger of
    // missing a compare match an generating a glitch.
    // We do not enable interrupts, because the motor is still standing
    TCCR1A = ( 1 << WGM11 ) | ( 1 << WGM10 );
    TCCR1B = ( 1 << WGM13 ) | ( 1 << WGM12 ) | ( 1 << CS10 ) | ( 1 << CS11 );
}


void SSSC_setSpeed(int32_t speed){
    StepperSetSpeed(&stepper, speed);
}


ISR (TIMER0_OVF_vect){ // Recalc timer
    struct StepperStepTime recalcResult;
    recalcResult = StepperRecalc(&stepper);
    if ( recalcResult.stepTimeMode == noStep ) {
#if defined ( __AVR_ATmega328P__ ) || defined ( __AVR_ATmega328__ )
        TIMSK1 &= ~( 1 << TOIE1 );
#elif defined  (__AVR_ATmega8__ )
        TIMSK &= ~( 1 << TOIE1 );
#endif
    }
    else if ( recalcResult.stepTimeMode == updateStepTime ) {
        // divide by 64 (step timer prescaler) to get timer ticks
        uint32_t stepTime = recalcResult.stepTime / 64;
        // limit step time to maximum supported by 16 bit timer
        if ( stepTime > UINT16_MAX ) {
            stepTime = UINT16_MAX;
        }
        OCR1A = stepTime;
        // enable step timer overflow interrupt if disabled when the stepper was stopped
#if defined ( __AVR_ATmega328P__ ) || defined ( __AVR_ATmega328__ )
        TIMSK1 |= ( 1 << TOIE1 );
#elif defined  (__AVR_ATmega8__ )
        TIMSK |= ( 1 << TOIE1 );
#endif
    }
    // if stepTime did not change, we do not need to do anything
}
ISR (TIMER1_OVF_vect){
    StepperStep(&stepper);
}

