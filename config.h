/* Configuration switches for the right ascension control.
 * Author: lukjp
 */

#include <stdint.h>

#ifndef CONFIG_H_
#define CONFIG_H_

// NOTE: The following constants can be changed

#define FOLLOW_SWITCH_PORT PORTC
#define FOLLOW_SWITCH_PIN PINC
#define FOLLOW_SWITCH_PIN_NR 5

#define MOON_SWITCH_PORT PORTC
#define MOON_SWITCH_PIN PINC
#define MOON_SWITCH_PIN_NR 4

#define OVERRIDE_ENABLE_PORT PORTC
#define OVERRIDE_ENABLE_PIN PINC
#define OVERRIDE_ENABLE_PIN_NR 3

#define OVERRIDE_POT_ADC_PIN 0

#define GEAR_TRANSMISSION_RATIO (144UL * 50 * 24)
#define STEPPER_STEP_COUNT 64

// override speed for the motor in steps per 1000 seconds at full speed
// NOTE: This is the speed for maximum control input, if the potentiometer is
// calibrated at half of the supply voltage, this speed is not achievable.
#define OVERRIDE_MAX_SPEED 1000000

// NOTE: The following constants should not be changed

#define SIDEREAL_DAY_s 86164
// Time for the moon to appear on almost the same spot as the day before
// approximately 24 hours and 50 minutes.
// This value is not accurate, but i could not find a better value and i am too
// lazy to compute it myself.
#define MOON_APPARENT_ORBIT_PERIOD_s (60 * (60UL * 24 + 50))

// Stepper speed to track the stars or the moon in steps per 1000 seconds
#define STARS_STEPPER_SPEED ((uint32_t) ((((uint64_t) 1000) * GEAR_TRANSMISSION_RATIO * STEPPER_STEP_COUNT) / (SIDEREAL_DAY_s)))
#define MOON_STEPPER_SPEED ((uint32_t) ((((uint64_t) 1000) * GEAR_TRANSMISSION_RATIO * STEPPER_STEP_COUNT) / (MOON_APPARENT_ORBIT_PERIOD_s)))


#endif // CONFIG_H_
